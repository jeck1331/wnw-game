import express from "express";
import { createServer } from "http";
import WebSocket from "ws";

const app = express();
export const server = createServer(app);
export const wss = new WebSocket.Server({ server });