import { wss } from "../shared/SocketConst.js";

export function GetWaitingUsers() {
    let clients = [];
    wss.clients.forEach(u => {
      if (u.status == "waitingGame") {
        clients.push(u);
      }
    });
    return clients;
}