import { wss } from "../shared/SocketConst.js";

export function AuthenticateUser(request, ws) {
    let userName = request.headers['www-authenticate'];
    wss.clients.forEach(w => {
        if (w.UserName == userName) {
          ws.send("0This user already logged");
          ws.close();
        }
    });
    if (ws.readyState == ws.CLOSED || ws.readyState == ws.CLOSING) return;
    console.log(userName + " has joined.");
    AssignUniqueUserId(ws);
    ws.UserName = userName;
  }
  
  function AssignUniqueUserId(ws) {
    let userId = 0;
    wss.clients.forEach(w => {
      if (w.UserId == userId) {
        userId++;
      } else {
        ws.UserId = userId;
        return;
      }
    });
  }