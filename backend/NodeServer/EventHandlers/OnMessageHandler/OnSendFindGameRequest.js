import { wss } from "../../shared/SocketConst.js";
import { maps } from "../../index.js";
import { GetWaitingUsers } from "../../scripts/GetWaitingUsers.js";

export function OnSendFindGameRequest (json, ws) {  
    if (json.findGame) {
        let numberOfPlayers = 2;

        let waitingUsers = GetWaitingUsers();
        assignMapToUser(waitingUsers, ws);

        ws.status = "waitingGame";
        waitingUsers.push(ws);
        sendOtherUsersThatYouConnected(waitingUsers);
        console.log(ws.UserName + " is waiting");
        
        if (waitingUsers.length == numberOfPlayers) {
            let randomCells = generateRandomCells(numberOfPlayers);
            writePlayersToMap(waitingUsers, randomCells);
            markAsPlaying(waitingUsers);
            waitingUsers.forEach(u => {
              u.send("2{ 'enemyCell': '" + u.CurrentCell + "', 'enemyId': '" + u.UserId + "' }")
            });
        }
    }
}


function assignMapToUser(waitingUsers, ws) {
  if (waitingUsers.length >= 1) {
    ws.CurrentMap = ws[0].CurrentMap;
  } else {
    let mapId = maps.push(CreateBlankMap());
    ws.CurrentMap = mapId - 1;
  }
}

function sendOtherUsersThatYouConnected(ws) {
  ws.send("3" + ws.length);
}

function generateRandomCells(numberOfPlayers) {
    let randomCells = [];
    for (let i = 0; i < numberOfPlayers; i++) {
      let cellNumber = -1;
      do {
        cellNumber = Math.round(Math.random() * 100);
      }
      while (randomCells.indexOf(cellNumber) !== -1);
      randomCells.push(cellNumber);
    }
    return randomCells;
}

function writePlayersToMap(waitingUsers, randomCells) {
  let mapId = waitingUsers[0].CurrentMap;
  for (let i = 0; i < waitingUsers.length; i++) {
    maps[mapId][randomCells[i]] = waitingUsers[i].UserId;
    waitingUsers[i].CurrentCell = randomCells[i];
  }
}

function markAsPlaying(waitingUsers) {
  waitingUsers.forEach(u => {
    u.status = "playing";
    console.log(u.UserName + " is playing on №" + u.CurrentMap + " map");
  });
}

function CreateBlankMap() {
  return [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
}