import { OnSendTextRequest } from "./OnSendTextRequest.js";
import { OnSendFindGameRequest } from "./OnSendFindGameRequest.js";
import { OnSendNextPositionRequest } from "./OnSendNextPositionRequest.js";
import { OnSendOutGameRequest } from "./OnSendOutGameRequest.js";
import { OnSendConfirmGameRequest } from "./OnSendConfirmGameRequest.js";

export function OnMessageHandler(data, ws) {
  const json = JSON.parse(data);
  
  OnSendTextRequest(data, ws);
  OnSendFindGameRequest(json, ws);
  OnSendNextPositionRequest(json, ws);
  OnSendOutGameRequest(json, ws);
  OnSendConfirmGameRequest(json, ws);
}