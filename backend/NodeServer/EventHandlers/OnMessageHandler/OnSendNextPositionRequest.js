import { maps } from "../../index.js";
import { wss } from "../../shared/SocketConst.js";

export function OnSendNextPositionRequest(json, ws) {
  if (json.nextPos !== undefined) {
    let mapId = json.mapId;
    let nextPos = json.currentPos;
    let nextCell = maps[mapId][nextPos];

    cleanPreviousPosition(mapId, ws);
    TapOnPreviousPlayer(nextCell, ws);
    maps[mapId][nextPos] = ws.UserId;
    
    ws.send("1" + JSON.stringify(maps[mapId]));
  }
}

function cleanPreviousPosition(mapId, ws) {
    for (let i = 0; i < maps[mapId].length; i++) {
        if (maps[mapId][i] == ws.UserId) {
            maps[mapId][i] = -1;
        }
    }
}

function TapOnPreviousPlayer(nextCell, ws) {
    if (nextCell !== 0 && nextCell !== ws.UserId) {
        wss.clients.forEach(c => {
            if (c.UserId == nextCell) {
                c.status = "lose";
            }
        });
    }
}
