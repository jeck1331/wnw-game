import { maps } from "../../index.js";
import { wss } from "../../shared/SocketConst.js";

export function OnSendOutGameRequest (json, ws) {
    if (json.outGame) {
        ws.status = "idle";
        sendUsersThatPlayerLeave(ws.CurrentMap, ws.UserId);
        ws.CurrentMap = null;
        console.log(ws.UserName + " left the game");
    }
}

function sendUsersThatPlayerLeave(mapId, leavedUserId) {
    let userIds = [];
    maps[mapId].forEach(u => {
        if (u !== -1) {
            userIds.push(u);
        }
    });

    wss.clients.forEach(c => {
        if (userIds.indexOf(c.UserId) !== -1) {
            c.send("5" + leavedUserId);
        }
    });
}
