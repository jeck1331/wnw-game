import {OnMessageHandler} from "./EventHandlers/OnMessageHandler/OnMessageHandler.js";
import {AuthenticateUser} from "./scripts/AuthenticateUser.js";
import {OnCloseHandler} from "./EventHandlers/OnCloseHandler.js";
import {server, wss} from "./shared/SocketConst.js";

export let maps=[];

wss.on('connection', function(ws, request) {
  AuthenticateUser(request, ws);

  ws.on('message', data => OnMessageHandler(data, ws));
  ws.on('close', code => OnCloseHandler(code, ws));
});

server.listen(8081, function() {
  console.log('Listening on http://localhost:8081');
});
