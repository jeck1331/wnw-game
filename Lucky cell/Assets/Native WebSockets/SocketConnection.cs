using System.Collections.Generic;
using UnityEngine;
using NativeWebSocket;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using Assets;
using Assets.Native_WebSockets;

public class SocketConnection : MonoBehaviour
{
    private GameManager _gm;

    void Start()
    {
        _gm = GameManager.Instance;
        EventAggregator.SendUserNameEvent += EnteredUserName;
        Requests.BindEvents();
    }

    public async void EnteredUserName(string username)
    {
        var headers = new Dictionary<string, string> {
            { "www-authenticate", username }
        };

        _gm.Websocket ??= new WebSocket("ws://localhost:8081", headers);

        _gm.Websocket.OnOpen += () =>
        {
            RequestAggregator.SendTextEventInvoke("Hello");
            EditorSceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
            Debug.Log("Connection open!");
        };

        _gm.Websocket.OnError += (e) =>
        {
            Debug.Log("Error! " + e);
        };

        _gm.Websocket.OnClose += (e) =>
        {
            EditorSceneManager.LoadScene("WelcomingScene", LoadSceneMode.Single);
            Debug.Log("Connection closed!");
        };

        _gm.Websocket.OnMessage += (bytes) =>
        {
            var response = Encoding.UTF8.GetString(bytes);

            if (response[0] == '0')
            {
                GetErrorResponse(response);
            }
            else
            {
                Responses.HandleByEvents(response);
            }
        };
        
        //InvokeRepeating("SendWebSocketMessage", 0.0f, 0.3f);
        await _gm.Websocket.Connect();
    }

    private void GetErrorResponse(string message)
    {
        foreach (var o in FindObjectsOfType(typeof(Text)))
        {
            if (o.name == "ModalTextDialog")
            {
                (o as Text).text = message;
                break;
            }
        }
    }

    //async void SendWebSocketMessage()
    //{
    //    if (websocket.State == WebSocketState.Open)
    //    {
    //        await websocket.Send(new byte[] { 10, 20, 30 });
    //        await websocket.SendText("plain text message");
    //    }
    //}

    private async void OnApplicationQuit()
    {
        await _gm.Websocket.Close();
    }
}
