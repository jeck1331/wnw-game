﻿using System.Linq;
using Newtonsoft.Json;

namespace Assets.Native_WebSockets
{
    public class Responses
    {
        public static void HandleByEvents(string response)
        {
            var json = string.Join("", response.Skip(1));

            switch (response[0])
            {
                case '1':
                    GetGameStatusResponse(json);
                    break;
                case '2':
                    GetEnemyStepResponse(json);
                    break;
                case '3':
                    GetConnectedUsersCount(json);
                    break;
                case '4':
                    GetUsersConfirmedGameCount(json);
                    break;
                case '5':
                    GetEnemyLeave(json);
                    break;
                default:
                    break;
            }
        }


        /// <summary>Автоматически получить статус пользователя<summary>
        private static void GetGameStatusResponse(string json) // принимает значения: "idle" | "waitingGame" | "playing" | "lose"
        {
            var response = JsonConvert.DeserializeObject<string>(json);
            ResponseAggregator.GetGameStatusEventInvoke(response);
        }

        /// <summary>Автоматически получить местоположение соперника<summary>
        private static void GetEnemyStepResponse(string json)
        {
            var response = JsonConvert.DeserializeObject<GetEnemyStepResponse>(json);
            ResponseAggregator.GetEnemyStepEventInvoke(response);
        }

        /// <summary>Автоматически получить количество подключенных игроков в комнате ожидания или в игре<summary>
        private static void GetConnectedUsersCount(string json)
        {
            var response = JsonConvert.DeserializeObject<int>(json);
            ResponseAggregator.GetConnectedUsersCountEventInvoke(response);
        }

        /// <summary>Автоматически получить сколько игроков приняло "начать игру не ожидая других игроков"</summary>
        private static void GetUsersConfirmedGameCount(string json)
        {
            var response = JsonConvert.DeserializeObject<int>(json);
            ResponseAggregator.GetUsersConfirmedGameCountEventInvoke(response);
        }

        /// <summary>Автоматически получить инфу о том, ливнули ли с игры противники</summary>
        private static void GetEnemyLeave(string json)
        {
            var response = JsonConvert.DeserializeObject<int>(json);
            ResponseAggregator.GetEnemyLeaveEventInvoke(response);
        }
    }
}
