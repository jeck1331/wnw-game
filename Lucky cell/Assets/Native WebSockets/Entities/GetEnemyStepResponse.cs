﻿using Newtonsoft.Json;

public class GetEnemyStepResponse
{
    [JsonProperty("enemyCell")]
    public int EnemyCell { get; set; }

    [JsonProperty("enemyId")]
    public int EnemyId { get; set; }
}