﻿
namespace Assets.Native_WebSockets.Entities
{
    public class Map
    {
        public int Id { get; set; }
        public int[] Cells { get; set; }
        public Map()
        {
            Cells = new int[]
            {
                 0,0,0,0,0,0,0,0,0,0, //1
                 0,0,0,0,0,0,0,0,0,0, //2
                 0,0,0,0,0,0,0,0,0,0, //3
                 0,0,0,0,0,0,0,0,0,0, //4
                 0,0,0,0,0,0,0,0,0,0, //5
                 0,0,0,0,0,0,0,0,0,0, //6
                 0,0,0,0,0,0,0,0,0,0, //7
                 0,0,0,0,0,0,0,0,0,0, //8
                 0,0,0,0,0,0,0,0,0,0, //9
                 0,0,0,0,0,0,0,0,0,0  //10
            };
        }
    }
}
