﻿using System.Text;

namespace Assets.Native_WebSockets
{
    public class Requests
    {
        public static void BindEvents()
        {
            RequestAggregator.SendTextEvent += SendTextRequest;
            RequestAggregator.SendFindGameEvent += SendFindGameRequest;
            RequestAggregator.SendNextPositionEvent += SendNextPositionRequest;
            RequestAggregator.SendOutGameEvent += SendOutGameRequest;
        }

        /// <summary>Отправить текст (разработка)</summary>
        private static async void SendTextRequest(string text)
        {
            await GameManager.Instance.Websocket.SendText(text);
        }

        /// <summary>Послать запрос на сервер, чтобы он начал искать игру</summary>
        private static async void SendFindGameRequest()
        {
            await GameManager.Instance.Websocket.Send(Encoding.UTF8.GetBytes("{ \"findGame\": \"true\" }"));
        }

        /// <summary>Запрос на сервер, посылает выбранную следующую позицию</summary>
        private static async void SendNextPositionRequest(int nextPos)
        {
            await GameManager.Instance.Websocket.Send(Encoding.UTF8.GetBytes("{ \"nextPos\": \"" + nextPos + "\" }"));
        }

        /// <summary>Запрос на сервер, сообщает о том, что ты вышел из игры в главное меню</summary>
        private static async void SendOutGameRequest()
        {
            await GameManager.Instance.Websocket.Send(Encoding.UTF8.GetBytes("{ \"outGame\": \"true\" }"));
        }

        /// <summary>Кнопка "начать игру не ожидая других игроков"</summary>
        private static async void SendConfirmGameRequest()
        {
            await GameManager.Instance.Websocket.Send(Encoding.UTF8.GetBytes("{ \"confirmGame\": \"true\" }"));
        }
    }
}
