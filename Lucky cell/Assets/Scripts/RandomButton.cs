using UnityEngine;
using UnityEngine.EventSystems;

// �� ���?
public class RandomButton : MonoBehaviour, IPointerClickHandler
{
    private GameObject[] players;
    private GameObject[] cells;
    private Transform[] spawnPoints;

    public void OnPointerClick(PointerEventData eventData)
    {
        ChooseSet(players.Length);
    }

    void Shuffle(GameObject[] deck)
    {
        for (int i = 0; i < deck.Length; i++)
        {
            GameObject temp = deck[i];
            int randomIndex = Random.Range(0, deck.Length);
            deck[i] = deck[randomIndex];
            deck[randomIndex] = temp;
        }
    }

    void ChooseSet(int numRequired)
    {
        int numToChoose = numRequired;
        Shuffle(cells);
        Shuffle(players);

        for (int numLeft = spawnPoints.Length; numLeft > 0; numLeft--)
        {

            float prob = (float)numToChoose / (float)numLeft;

            if (Random.value <= prob)
            {
                numToChoose--;
                players[numToChoose].transform.position = cells[numLeft - 1].transform.position + new Vector3(0, 0, -3);

                if (numToChoose == 0)
                {
                    break;
                }
            }
        }
    }
}
