using System;
using UnityEngine;

public class EventAggregator
{
    public static event Action<Transform> CellSelectedEvent;
    public static void CellSelectedEventInvoke(Transform obj) => CellSelectedEvent?.Invoke(obj);


    public static event Action<string> SendUserNameEvent;
    public static void SendUserNameEventInvoke(string name) => SendUserNameEvent?.Invoke(name);
}

public class RequestAggregator
{
    public static event Action<string> SendTextEvent;
    public static void SendTextEventInvoke(string text) => SendTextEvent?.Invoke(text);


    public static event Action SendFindGameEvent;
    public static void SendFindGameEventInvoke() => SendFindGameEvent?.Invoke();



    public static event Action<int> SendNextPositionEvent;
    public static void SendNextPositionEventInvoke(int nextPos) => SendNextPositionEvent?.Invoke(nextPos);


    public static event Action SendOutGameEvent;
    public static void SendOutGameEventInvoke() => SendOutGameEvent?.Invoke();
}

public class ResponseAggregator
{
    public static event Action<string> GetGameStatusEvent;
    public static void GetGameStatusEventInvoke(string status) => GetGameStatusEvent?.Invoke(status);


    public static event Action<GetEnemyStepResponse> GetEnemyStepEvent;
    public static void GetEnemyStepEventInvoke(GetEnemyStepResponse obj) => GetEnemyStepEvent?.Invoke(obj);


    public static event Action<int> GetConnectedUsersCountEvent;
    public static void GetConnectedUsersCountEventInvoke(int count) => GetConnectedUsersCountEvent?.Invoke(count);


    public static event Action<int> GetUsersConfirmedGameCountEvent;
    public static void GetUsersConfirmedGameCountEventInvoke(int count) => GetUsersConfirmedGameCountEvent?.Invoke(count);


    public static event Action<int> GetEnemyLeaveEvent;
    public static void GetEnemyLeaveEventInvoke(int enemyId) => GetEnemyLeaveEvent?.Invoke(enemyId);
}