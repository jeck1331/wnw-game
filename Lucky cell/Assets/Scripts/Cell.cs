using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    private int _id;
    private int _currentPlayerId;

    private void Awake()
    {
        _id = -1;
        _currentPlayerId = -1;
    }

    public void SetId(int num)
    {
        _id = num;
    }
    public int GetId()
    {
        return _id;
    }

    public int GetCurrentPlayerId()
    {
        return _currentPlayerId;
    }
}
