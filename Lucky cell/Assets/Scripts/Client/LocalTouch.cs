using UnityEngine;
using UnityEngine.EventSystems;

public class LocalTouch : MonoBehaviour, IPointerClickHandler
{
    private GameObject lastSelectedCell;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (lastSelectedCell == null)
        {
            lastSelectedCell = eventData.pointerEnter;
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 1f, 1f);
        }
        else if (lastSelectedCell == eventData.pointerEnter)
        {
            EventAggregator.CellSelectedEventInvoke(lastSelectedCell.transform);
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1f);
        }
        else
        {
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1f);
            lastSelectedCell = eventData.pointerEnter;
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 1f, 1f);
        }
    }
}
