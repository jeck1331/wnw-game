﻿using NativeWebSocket;
using UnityEngine;

namespace Assets
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        public WebSocket Websocket { get; set; }

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
