using System;
using UnityEngine;
using UnityEngine.UI;

public class EnterUserName : MonoBehaviour
{
    public void OnClick()
    {
        var userInput = GetUserNameInput();
        var textDialog = GetDialogWindow();
        if (textDialog.text == "")
        {
            textDialog.text = "Сервер недоступен!";
        }
        if (!ValidateUserName(userInput.text))
        {
            userInput.text = "Incorrect UserName";
        }
        else
        {
            EventAggregator.SendUserNameEventInvoke(userInput.text);
        }
    }

    private static bool ValidateUserName(string userName)
    {
        if (string.IsNullOrWhiteSpace(userName))
            return false;

        return true;
    }

    private static InputField GetUserNameInput()
    {
        foreach (var input in FindObjectsOfType(typeof(InputField)))
        {
            if (input.name == "UserNameInput")
            {
                return (input as InputField);
            }
        }
        throw new Exception("UserNameInput not found");
    }

    private static Text GetDialogWindow()
    {
        foreach (var TextD in FindObjectsOfType(typeof(Text)))
        {
            if (TextD.name == "ModalTextDialog")
            {
                return (TextD as Text);
            }
        }
        throw new Exception("UserNameInput not found");
    }
}
