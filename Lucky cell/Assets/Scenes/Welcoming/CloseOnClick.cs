using UnityEngine;
using UnityEngine.EventSystems;

public class CloseOnClick : MonoBehaviour, IPointerClickHandler
{
    [System.Obsolete]
    public void OnPointerClick(PointerEventData eventData)
    {
        gameObject.active = false;
    }
}
