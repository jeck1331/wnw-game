using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishRound : MonoBehaviour
{
    public GameObject _text;
    private int CountLive = 1;
    private bool isDead = false;
    private int _timer = 0;
    private int _timeSleep = 5;

    void Start()
    {
        _timer = DateTime.Now.Second;
        if (CountLive > 1 && !isDead)
        {
            _text.GetComponent<Text>().text = $"В живых осталось\n {CountLive}";
        }

        if (isDead)
        {
            _text.GetComponent<Text>().text = "На вас прыгнул другой игрок и вы погибли.\nВ следующий раз повезёт!";
        }

        if (CountLive == 1)
        {
            _text.GetComponent<Text>().text = "Вы выиграли!";
        }
        
    }

    void Update()
    {
        if (_timeSleep == DateTime.Now.Second - _timer && !isDead)
        {
            SceneManager.LoadSceneAsync("MenuScene");
        }
        else if (_timeSleep == DateTime.Now.Second - _timer && isDead)
        {
            SceneManager.LoadSceneAsync("WelcomingScene");
        }
    }
}
