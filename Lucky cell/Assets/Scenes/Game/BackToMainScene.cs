using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMainScene : MonoBehaviour
{
    public void BackToMenu()
    {
        SceneManager.LoadSceneAsync("MenuScene", LoadSceneMode.Single);
    }
}
