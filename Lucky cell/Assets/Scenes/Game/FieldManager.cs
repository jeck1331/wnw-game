using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class FieldManager : MonoBehaviour, IPointerClickHandler
{
    private GameObject[] _cells;
    private List<GameObject> _players;
    private int[] cells;
    private GameObject testPlayer;
    private bool Winner = false;

    [SerializeField] private int _cellsCount;
    [SerializeField] private int _playerCount;
    [SerializeField] private GameObject _cell;
    [SerializeField] private GameObject _player;

    private GameObject lastSelectedCell;

    private void Start()
    {
        //ResponseAggregator.GetEnemyStepEvent += FillCells;/////////////////
        //ResponseAggregator.GetEnemyStepEvent += GeneratePlayerOnField;                                                  
        _cells = new GameObject[_cellsCount];
        _players = new List<GameObject>();
        GenerateField(_cellsCount, _playerCount);
    }

    private void Update()
    {
        if (Winner)
        {
            SceneManager.LoadScene("FinishRoundScene");
        }
    }

    // private void FillCells(GetEnemyStepResponse obj)
    // {
    //     // TODO: ������� ���� ���������� ������ 
    //     for (int numLeft = _cellsCount; numLeft > 0; numLeft--)
    //     {
    //
    //         float prob = (float)numToChoose / (float)numLeft;
    //
    //         if (Random.value <= prob)
    //             if (_players[numToChoose] != _mainPlayer)
    //             {
    //                 numToChoose--;
    //                 _players[numToChoose].transform.position = _cells[numLeft - 1].transform.position + new Vector3(0, 0, -3);
    //                 _players[numToChoose].GetComponent<Unit>().SetCurrentCellId(_cells[numLeft - 1].GetComponent<Cell>().GetId());
    //
    //                 if (numToChoose == 0)
    //                     float prob = (float)numToChoose / (float)numLeft;
    //
    //                 if (Random.value <= prob)
    //                 {
    //                     break;
    //                     numToChoose--;
    //                     _players[numToChoose].transform.position = _cells[numLeft - 1].transform.position + new Vector3(0, 0, -3);
    //                     _players[numToChoose].GetComponent<Unit>().SetCurrentCellId(_cells[numLeft - 1].GetComponent<Cell>().GetId());
    //
    //                     if (numToChoose == 0)
    //                     {
    //                         break;
    //                     }
    //                 }
    //             }
    //     }
    // }

    private void GenerateField(int cellsCount, int playerCount)
    {
        if (cellsCount < 4 || cellsCount > 126)
        {
            Debug.LogError("Invalid range of cells");
            return;
        }

        if (playerCount < 0 || playerCount > 50)
        {
            Debug.LogError("Invalid range of players");
            return;
        }

        for (int i = 0; i < cellsCount; i++)
        {
            _cells[i] = Instantiate(_cell, transform, true);
            _cells[i].name = "Cell_" + i;
            _cells[i].transform.position = new Vector3(-4f + (float)(i % 9), 6.4f - (float)(i / 9), -2);
            _cells[i].GetComponent<Cell>().SetId(i);
        }
        for (int i = 0; i < playerCount; i++)
        {
            _players.Add(Instantiate(_player));
            _players[i].name = "user" + i;
            if (i == 0)
            {
                testPlayer = _players[i];
            }
            _players[i].GetComponent<Unit>().SetId(i);
        }
        SetPlayersOnField();
    }

    // private void GeneratePlayerOnField(GetEnemyStepResponse sp)
    // {
    //     Transform vc = _cells.First(x => x.GetComponent<Cell>().GetId() == sp.EnemyCell).transform;
    //     _mainPlayer = Instantiate(_player, vc);
    // }
    
    private void SetPlayersOnField()
    {
        int numToChoose = _playerCount;
        // TODO: Shuffle

        for (int numLeft = _cellsCount; numLeft > 0; numLeft--)
        {
            float prob = (float) numToChoose / (float) numLeft;

            if (Random.value <= prob)
            {
                numToChoose--;
                _players[numToChoose].transform.position =
                    _cells[numLeft - 1].transform.position + new Vector3(0, 0, -3);
                _players[numToChoose].GetComponent<Unit>()
                    .SetCurrentCellId(_cells[numLeft - 1].GetComponent<Cell>().GetId());

                if (numToChoose == 0)
                {
                    break;
                }
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (lastSelectedCell == null)
        {
            lastSelectedCell = eventData.pointerEnter;
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.33f, 0.93f, 0.75f);
        }
        else if (lastSelectedCell == eventData.pointerEnter)
        {
            for (int i = 0; i < _playerCount; i++)
            {
                if (_players[i])
                    if (lastSelectedCell.transform.position.x == _players[i].transform.position.x &&
                        lastSelectedCell.transform.position.y == _players[i].transform.position.y)
                    {
                        {
                            if (testPlayer.name != _players[i].name)
                            {
                                Destroy(_players[i]);
                                _players.Remove(_players[i]);
                                _playerCount = _players.Count;
                                Winner = _playerCount == 1 ? true : false;
                            }
                        }
                    }
            }

            //EventAggregator.CellSelectedEventInvoke(lastSelectedCell.transform);
            testPlayer.transform.position = new Vector3(lastSelectedCell.transform.position.x,
                lastSelectedCell.transform.position.y, testPlayer.transform.position.z);
            
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.33f, 0.93f, 0.75f);
        }
        else
        {
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0.83f, 0.33f, 0.93f, 0.75f);
            lastSelectedCell = eventData.pointerEnter;
            lastSelectedCell.GetComponent<SpriteRenderer>().color = new Color(0.83f, 0.33f, 0.93f, 0.75f);
        }
    }
}
