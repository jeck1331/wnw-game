using UnityEngine;

public class Unit : MonoBehaviour
{
    private int _id;
    private int _currentCellId;
    void Awake()
    {
        EventAggregator.CellSelectedEvent += MoveToCell;
        _id = -1;
        _currentCellId = -1;
    }

    public void SetId(int num)
    {
        _id = num;
    }
    public int SGetId()
    {
        return _id;
    }

    public void SetCurrentCellId(int num)
    {
        _id = num;
    }
    public int GetCurrentCellId()
    {
        return _id;
    }

    public void MoveToCell(Transform cellPosition)
    {
        transform.position = cellPosition.position + new Vector3(0, 0, -3);
        _currentCellId = cellPosition.GetComponent<Cell>().GetId();

    }

}
