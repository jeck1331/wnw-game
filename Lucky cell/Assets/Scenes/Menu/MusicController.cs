using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{
    public GameObject btn;
    private bool isActive = true;
    public void ChangeValue()
    {
        if (isActive)
        {
            isActive = !isActive;
            btn.GetComponent<Image>().color = new Color(0.96f, 0.34f, 0.24f, 1f);
            btn.GetComponent<AudioSource>().Stop();
        }
        else
        {
            isActive = !isActive;
            btn.GetComponent<Image>().color = new Color(0f, 1f, 0.29f, 1f);
            btn.GetComponent<AudioSource>().Play();
        }
    }
}
