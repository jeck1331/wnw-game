using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    //private string status = "";

    private void Start()
    {
        //ResponseAggregator.GetGameStatusEvent += GameStatus;
    }

    public void StartEvent()
    {
        //if (status.Length <= 0)
        //{
           // RequestAggregator.SendFindGameEventInvoke();  
        //}
        
        //if (status == "playing")
        //{
        SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Single);
        // }

    }

    // private void GameStatus(string response)
    // {
    //     status = response;
    // }

    public void QuitEvent()
    {
        Application.Quit();
    }
}
